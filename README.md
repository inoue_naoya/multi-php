# このDocker環境でできること
## multi-php v-0.1.1
PHPのバージョンを複数構築し、1つのソースを複数のPHPバージョンで実行させることを目的として構築

- **Apache2.4.x(Debian)**
    - Debian系なので、設定ファイルが少し違う（業務では主にRedHat系）
- **PHP 7.3.x**
    - 7.3系の最新バージョンをDockerHubよりプルする
- **PHP 7.4.x**
    - 7.4系の最新バージョンをDockerHubよりプルする
- **MySQL 5.7**
- **phpMyAdmin**
- **composer 1.8.5**
    - バージョン1.8.5にしているのは moka インストール時に影響するため
- **実行ディレクトリの共通化**
    - `workspace\www` 以下をマウント
    - ドキュメントルートは `workspace\www\html`
- **SMTPサーバー**として**Mailhog**
    - sendmail関数利用可能

# 準備
## <u>01.C直下などに当外ディレクトリを配置</u>
<br>

**※もし以下のローカルループバックアドレス(`127.0.0.*`)以外を利用する場合**<br>
　各個人で、`Docker-Compose.yml`含め置き換えてください<br>
　`127.0.0.1`は利用しない方が良いかと（XAMPPとか）
<details>
<summary>Macのみ開放する必要あり</summary>

>PHP7.3.x用<br>
```sudo ifconfig lo0 alias 127.1.10.73 up```<br>

>PHP7.4.x用<br>
```sudo ifconfig lo0 alias 127.1.10.74 up```<br>

>MySQL用<br>
```sudo ifconfig lo0 alias 127.1.1.1 up```<br>

>phpMyAdmin用<br>
```sudo ifconfig lo0 alias 127.1.1.2 up```<br>

>Mailhog用<br>
```sudo ifconfig lo0 alias 127.1.1.10 up```<br>
</details>
<br>

## <u>02.WinとMac共通hostsファイル加筆</u>
<details>
<summary>（メモ）ディレクトリ</summary>

| OS | Dir |
| ---- | ---- |
| Mac | /private/etc/hosts |
|Windows| C:\Windows\System32\drivers\etc\hosts |

</details>

```
127.1.1.1                   dev-mysql
127.1.1.2                   dev-phpmyadmin
127.1.1.10                  dev-mailhog
127.1.10.73                 dev-php73
127.1.10.74                 dev-php74
```

# 実行
## <u>03.PowerShellもしくはTerminalで配置したディレクトリに移動</u>
`docker-compose build --no-cache`<br>
`docker-compose up -d`

## <u>04.起動確認</u>
### hostsで指定したドメインにアクセス

- dev-phpmyadmin
    - phpmyadminに入れたらOK
- dev-php73
- dev-php74
    - バージョンが別であればOK<br>
    `workspace\www\html\index.php` で `php_info()`を実行
- dev-mailhog
    - Mailhogには入れたらOK

## <u>05.起動したコンテナに入る</u>
### PHP7.3.x用
`docker exec -it multi-php_php73_1 bash`

### PHP7.4.x用
`docker exec -it multi-php_php74_1 bash`

**各種コマンドはコンテナ内で実行**
**`composer`や`Artisan`コマンドはどちらかで行えばOK**

## <u>06.Mailhogの情報</u>
Laravelのenvで書く場合
```
MAIL_DRIVER=smtp
MAIL_HOST=mailhog
MAIL_PORT=1025
MAIL_ENCRYPTION=
```

# 今後
01. ApacheをDebian系からRedHat系へ

# 確認OS
- Windows 10 Pro 1909
- Mac OS Catalina 10.15.7

# 更新履歴
- 2021-03-27：PHPの拡張intlを利用可能に、CakePHPを利用可能にするため
- 2021-02-07：MySQLのlogファイルマウントの修正、サンプル記述のhosts内容修正
- 2021-02-06：mailhogの設定をphp7.4に反映忘れを修正
- 2021-02-01：ローカルループバックアドレスの変更（MySQL追加、他変更）
- 2021-01-29：メール送信（sendmail関数）をmailhogで利用可能になりました。